package com.example.myapplication.data.repositories

import com.example.myapplication.data.models.toModel
import com.example.myapplication.data.network.UnsplashApi
import com.example.myapplication.domain.models.Image
import com.example.myapplication.domain.repositories.ImagesRepository
import io.reactivex.Single

class ImagesRepositoryImpl(private val api: UnsplashApi) : ImagesRepository {
	override fun loadImages(page: Int, quantity: Int): Single<List<Image>> {
		return api
			.loadPhotos(page, quantity)
			.map { images ->
				images.map { it.toModel() }
			}
	}
}