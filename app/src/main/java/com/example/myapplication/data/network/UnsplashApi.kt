package com.example.myapplication.data.network

import com.example.myapplication.data.models.ImageDO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashApi {

	@GET("/photos")
	fun loadPhotos(
		@Query("page") page: Int,
		@Query("per_page") perPage: Int
	): Single<List<ImageDO>>
}