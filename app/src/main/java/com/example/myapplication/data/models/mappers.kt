package com.example.myapplication.data.models

import com.example.myapplication.domain.models.Image

fun ImageDO.toModel(): Image = Image(id, urls.small, urls.full)