package com.example.myapplication.data.models

import com.google.gson.annotations.SerializedName


data class ImageDO(
	@SerializedName("id") val id: String,
	@SerializedName("urls") val urls: Urls
)

data class Urls(
	@SerializedName("full") val full: String,
	@SerializedName("small") val small: String
)