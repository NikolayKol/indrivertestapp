package com.example.myapplication.presentation.gallery.epoxy

import android.content.Context
import android.widget.FrameLayout
import com.airbnb.epoxy.ModelView
import com.example.myapplication.R

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ProgressView(context: Context) : FrameLayout(context) {
	init {
		inflate(context, R.layout.layout_loading, this)
	}
}