package com.example.myapplication.presentation.base.mvi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.presentation.base.app.GalleryApp
import com.example.myapplication.presentation.base.di.components.PresentersComponent
import io.reactivex.subjects.BehaviorSubject

@Suppress("UNCHECKED_CAST")
abstract class BaseMVIActivity<Intent, View : BaseMVIView<Intent>, Presenter : BaseMVIPresenter<View, Intent>> :
	AppCompatActivity(), BaseMVIView<Intent> {

	final override val intents = BehaviorSubject.create<Intent>()
	abstract val presenter: Presenter

	val presentersComponent: PresentersComponent
		get() = (applicationContext as GalleryApp).presentersComponent

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		presenter.attach(this as View)
	}

	override fun onDestroy() {
		super.onDestroy()
		presenter.detach()
	}
}