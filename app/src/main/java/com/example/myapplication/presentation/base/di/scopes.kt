package com.example.myapplication.presentation.base.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PresentersScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class NavigationScope