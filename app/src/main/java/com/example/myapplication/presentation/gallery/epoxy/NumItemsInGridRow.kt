package com.example.myapplication.presentation.gallery.epoxy

import com.airbnb.epoxy.EpoxyModel

class NumItemsInGridRow
private constructor(val numItemsForCurrentOrientation: Int) :
	EpoxyModel.SpanSizeOverrideCallback {
	override fun getSpanSize(totalSpanCount: Int, position: Int, itemCount: Int): Int {
		return totalSpanCount / numItemsForCurrentOrientation
	}

	companion object {
		fun forPortrait() = NumItemsInGridRow(2)

		fun forLandscape() = NumItemsInGridRow(4)
	}
}