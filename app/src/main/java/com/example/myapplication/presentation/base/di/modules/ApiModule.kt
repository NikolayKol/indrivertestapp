package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.data.network.UnsplashApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ApiModule {
	@Provides
	fun unsplashApi(retrofit: Retrofit): UnsplashApi {
		return retrofit.create(UnsplashApi::class.java)
	}
}