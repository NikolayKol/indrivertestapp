package com.example.myapplication.presentation.base.mvi

import io.reactivex.Completable
import io.reactivex.subjects.PublishSubject


abstract class BaseMVIPresenter<View : BaseMVIView<Intent>, Intent> {

	abstract val viewState: BaseViewState<View, Intent>
	protected val intents = PublishSubject.create<Intent>()

	abstract fun execute(): Completable

	fun attach(view: View) {
		view.intents.subscribe(intents)
		viewState.attach(view)
	}

	fun detach() {
		viewState.detach()
	}

}

abstract class BaseViewState<View : BaseMVIView<Intent>, Intent> {

	protected var view: View? = null

	open fun attach(view: View) {
		this.view = view
	}

	open fun detach() {
		this.view = null
	}
}