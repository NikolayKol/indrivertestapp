package com.example.myapplication.presentation.base.di.components

import com.example.myapplication.domain.usecases.ImagesUseCases
import com.example.myapplication.presentation.base.di.modules.ApiModule
import com.example.myapplication.presentation.base.di.modules.RepositoriesModule
import dagger.Component

@Component(
	dependencies = [AppComponent::class],
	modules = [ApiModule::class, RepositoriesModule::class]
)
interface UseCaseComponent {
	fun imagesUseCases(): ImagesUseCases
}