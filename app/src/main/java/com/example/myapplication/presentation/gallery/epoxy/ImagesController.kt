package com.example.myapplication.presentation.gallery.epoxy

import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyModel
import com.example.myapplication.domain.models.Image

class ImagesController(
	private val imageClickListener: (Image) -> Unit,
	private val spanSizeOverrideCallback: EpoxyModel.SpanSizeOverrideCallback
) : EpoxyController() {

	var showLoading: Boolean = false
		set(value) {
			field = value
			requestModelBuild()
		}

	/**
	 * show network error only if retryLambda non null
	 */
	var retryLambda: (() -> Unit)? = null
		set(value) {
			field = value
			requestModelBuild()
		}

	var images: List<Image> = emptyList()
		set(value) {
			field = value
			requestModelBuild()
		}

	override fun buildModels() {

		images.forEach { image ->
			ImageViewModel_()
				.id(image.id)
				.imageUrl(image.smallImage)
				.listener { imageClickListener.invoke(image) }
				.spanSizeOverride(spanSizeOverrideCallback)
				.addTo(this)
		}

		ProgressViewModel_()
			.id(LOADING_VIEW_ID)
			.addIf(showLoading, this)

		ErrorViewModel_()
			.id(ERROR_VIEW_ID)
			.listener { retryLambda?.invoke() }
			.addIf(retryLambda != null, this)

	}

	companion object {
		private const val ERROR_VIEW_ID = "error_view"
		private const val LOADING_VIEW_ID = "loading_view"
	}
}