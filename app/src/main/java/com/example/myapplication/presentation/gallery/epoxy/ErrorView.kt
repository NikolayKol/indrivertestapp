package com.example.myapplication.presentation.gallery.epoxy

import android.content.Context
import android.widget.FrameLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.example.myapplication.R
import kotlinx.android.synthetic.main.layout_error.view.*

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ErrorView(context: Context) : FrameLayout(context) {

	private var listener: (() -> Unit)? = null

	init {
		inflate(context, R.layout.layout_error, this)
		retryBtn.setOnClickListener { listener?.invoke() }
	}

	@ModelProp(options = [ModelProp.Option.DoNotHash])
	fun listener(listener: () -> Unit) {
		this.listener = listener
	}

}