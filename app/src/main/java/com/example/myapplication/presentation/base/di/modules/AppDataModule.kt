package com.example.myapplication.presentation.base.di.modules

import android.content.Context
import com.example.myapplication.R
import com.example.myapplication.presentation.base.di.ACCESS_KEY_NAME
import com.example.myapplication.presentation.base.di.HOST_NAME
import dagger.Module
import dagger.Provides
import javax.inject.Named

@Module
class AppDataModule(private val context: Context) {
	@Provides
	fun context(): Context = context

	@Provides
	@Named(HOST_NAME)
	fun hostHame(): String {
		return context.getString(R.string.api_host)
	}

	@Provides
	@Named(ACCESS_KEY_NAME)
	fun accessKey(): String {
		return context.getString(R.string.access_key)
	}
}