package com.example.myapplication.presentation.base.di.components

import com.example.myapplication.presentation.base.di.NavigationScope
import com.example.myapplication.presentation.base.di.modules.NavigationModule
import dagger.Component
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@NavigationScope
@Component(modules = [NavigationModule::class])
interface NavigationComponent {
	fun router(): Router

	fun navigatorHolder(): NavigatorHolder
}