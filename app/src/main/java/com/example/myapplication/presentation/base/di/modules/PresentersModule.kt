package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.domain.usecases.ImagesUseCases
import com.example.myapplication.presentation.base.di.PresentersScope
import com.example.myapplication.presentation.gallery.GalleryPresenter
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class PresentersModule {
	@Provides
	@PresentersScope
	fun galleryPresenter(imagesUseCases: ImagesUseCases, router: Router): GalleryPresenter {
		return GalleryPresenter(router, imagesUseCases).apply {
			execute().subscribe()
		}
	}
}