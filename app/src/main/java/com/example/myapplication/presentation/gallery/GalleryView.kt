package com.example.myapplication.presentation.gallery

import com.example.myapplication.domain.models.Image
import com.example.myapplication.presentation.base.mvi.BaseMVIView

interface GalleryView : BaseMVIView<GalleryView.GalleryIntent> {

	fun showNetworkError(show: Boolean)

	fun showImages(images: List<Image>)

	fun showLoading(show: Boolean)

	sealed class GalleryIntent {
		object Start : GalleryIntent()
		object NextPage : GalleryIntent()
		data class ImageSelected(val image: Image) : GalleryIntent()
	}
}