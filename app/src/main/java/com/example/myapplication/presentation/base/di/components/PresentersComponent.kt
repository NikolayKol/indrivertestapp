package com.example.myapplication.presentation.base.di.components

import com.example.myapplication.presentation.base.di.PresentersScope
import com.example.myapplication.presentation.base.di.modules.PresentersModule
import com.example.myapplication.presentation.gallery.GalleryPresenter
import dagger.Component

@PresentersScope
@Component(
	dependencies = [UseCaseComponent::class, NavigationComponent::class],
	modules = [PresentersModule::class]
)
interface PresentersComponent {
	fun galleryPresenter(): GalleryPresenter
}