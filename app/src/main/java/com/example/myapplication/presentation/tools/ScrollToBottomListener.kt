package com.example.myapplication.presentation.tools

import androidx.recyclerview.widget.RecyclerView

class ScrollToBottomDetector(private val onScrollToBottom: () -> Unit) :
	RecyclerView.OnScrollListener() {
	override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
		if (!recyclerView.canScrollVertically(1)) {
			onScrollToBottom.invoke()
		}
	}
}