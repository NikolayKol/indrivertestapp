package com.example.myapplication.presentation.gallery

import com.example.myapplication.domain.models.Image
import com.example.myapplication.domain.usecases.ImagesUseCases
import com.example.myapplication.presentation.base.mvi.BaseMVIPresenter
import com.example.myapplication.presentation.base.mvi.BaseViewState
import com.example.myapplication.presentation.base.navigation.ImageScreen
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.terrakok.cicerone.Router
import java.io.IOException

class GalleryPresenter(
	private val router: Router,
	private val imagesUseCases: ImagesUseCases
) : BaseMVIPresenter<GalleryView, GalleryView.GalleryIntent>() {

	private val viewStateScheduler = AndroidSchedulers.mainThread()
	private val ioScheduler = Schedulers.newThread()

	override val viewState = GalleryViewState()

	override fun execute(): Completable = Observable
		.mergeArray(
			intents
				.ofType(GalleryView.GalleryIntent.Start::class.java)
				.observeOn(viewStateScheduler)
				.doOnNext {
					viewState.clearState()
					viewState.nextPageLoading()
				}
				.observeOn(ioScheduler)
				.flatMap { loadNextPage() },
			intents
				.ofType(GalleryView.GalleryIntent.ImageSelected::class.java)
				.observeOn(viewStateScheduler)
				.doOnNext {
					router.navigateTo(ImageScreen(it.image.bigImage))
				},
			intents
				.ofType(GalleryView.GalleryIntent.NextPage::class.java)
				.observeOn(viewStateScheduler)
				.doOnNext { viewState.nextPageLoading() }
				.observeOn(ioScheduler)
				.flatMap { loadNextPage() }
		)
		.ignoreElements()

	private fun loadNextPage(): Observable<Unit> {
		return imagesUseCases
			.getImagesPage(viewState.page)
			.toObservable()
			.observeOn(viewStateScheduler)
			.doOnNext {
				viewState.nextPageLoaded(it)
			}
			.doOnError {
				if (it is IOException) {
					viewState.nextPageLoadFailed()
				} else {
					//TODO show toast
				}
			}
			.map { Unit }
			.onErrorReturnItem(Unit)
	}

	class GalleryViewState : BaseViewState<GalleryView, GalleryView.GalleryIntent>() {

		private var state: State = State()
			set(value) {
				field = value
				view?.showImages(state.images)
				view?.showLoading(state.showLoading)
				view?.showNetworkError(state.showNetworkError)
			}

		override fun attach(view: GalleryView) {
			super.attach(view)
			view.showImages(state.images)
			view.showLoading(state.showLoading)
			view.showNetworkError(state.showNetworkError)
		}

		fun clearState() {
			state = State()
		}

		fun nextPageLoading() {
			state = state.copy(showLoading = true, showNetworkError = false)
		}

		fun nextPageLoaded(images: List<Image>) {
			state = state.copy(
				page = state.page + 1,
				images = mutableListOf<Image>().apply { addAll(state.images); addAll(images) },
				showLoading = false,
				showNetworkError = false
			)
		}

		fun nextPageLoadFailed() {
			state = state.copy(
				showLoading = false,
				showNetworkError = true
			)
		}

		val page
			get() = state.page

		data class State(
			val page: Int = 0,
			val images: List<Image> = emptyList(),
			val showNetworkError: Boolean = false,
			val showLoading: Boolean = false
		)
	}
}