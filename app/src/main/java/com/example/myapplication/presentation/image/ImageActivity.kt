package com.example.myapplication.presentation.image

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_image.*

class ImageActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_image)
		val imageUrl = intent.getStringExtra(IMAGE_URL_KEY)
		Picasso
			.get()
			.load(imageUrl)
			.into(imageView)
	}

	companion object {

		private const val IMAGE_URL_KEY = "imageUrl"

		fun createIntent(context: Context, imageUrl: String): Intent {
			return Intent(context, ImageActivity::class.java).apply {
				putExtra(IMAGE_URL_KEY, imageUrl)
			}
		}

	}
}