package com.example.myapplication.presentation.gallery.epoxy

import android.content.Context
import android.widget.FrameLayout
import com.airbnb.epoxy.ModelProp
import com.airbnb.epoxy.ModelView
import com.example.myapplication.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.layout_image.view.*

@ModelView(autoLayout = ModelView.Size.MATCH_WIDTH_WRAP_HEIGHT)
class ImageView(context: Context) : FrameLayout(context) {

	private var listener: (() -> Unit)? = null

	init {
		inflate(context, R.layout.layout_image, this)
		image.setOnClickListener { listener?.invoke() }
	}

	@ModelProp
	fun imageUrl(imageUrl: String) {
		Picasso
			.get()
			.load(imageUrl)
			.into(image)
	}

	@ModelProp(options = [ModelProp.Option.DoNotHash])
	fun listener(listener: () -> Unit) {
		this.listener = listener
	}
}