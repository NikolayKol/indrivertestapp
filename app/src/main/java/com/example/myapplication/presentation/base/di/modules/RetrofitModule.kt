package com.example.myapplication.presentation.base.di.modules

import android.content.Context
import com.example.myapplication.presentation.base.di.ACCESS_KEY_NAME
import com.example.myapplication.presentation.base.di.HOST_NAME
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named


@Module
class RetrofitModule(private val context: Context) {

	@Provides
	fun retrofit(@Named(HOST_NAME) host: String, @Named(ACCESS_KEY_NAME) accessKey: String): Retrofit {
		val interceptor = HttpLoggingInterceptor()
		interceptor.apply { interceptor.level = HttpLoggingInterceptor.Level.BODY }

		val httpClient = OkHttpClient
			.Builder()
			.addInterceptor { chain ->
				val original = chain.request()
				val originalHttpUrl = original.url

				val url = originalHttpUrl.newBuilder()
					.addQueryParameter("client_id", accessKey)
					.build()

				val requestBuilder = original.newBuilder()
					.url(url)

				val request = requestBuilder.build()
				chain.proceed(request)
			}
			.addInterceptor(interceptor)
			.build()


		return Retrofit
			.Builder()
			.baseUrl(host)
			.client(httpClient)
			.addConverterFactory(GsonConverterFactory.create())
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.build()
	}

}