package com.example.myapplication.presentation.base.di.components

import android.content.Context
import com.example.myapplication.presentation.base.di.ACCESS_KEY_NAME
import com.example.myapplication.presentation.base.di.HOST_NAME
import com.example.myapplication.presentation.base.di.modules.AppDataModule
import com.example.myapplication.presentation.base.di.modules.RetrofitModule
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Named
@Component(modules = [RetrofitModule::class, AppDataModule::class])
interface AppComponent {
	fun retrofit(): Retrofit

	fun context(): Context

	@Named(HOST_NAME)
	fun host(): String

	@Named(ACCESS_KEY_NAME)
	fun accessKey(): String
}