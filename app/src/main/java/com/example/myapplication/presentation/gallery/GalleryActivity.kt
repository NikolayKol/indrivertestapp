package com.example.myapplication.presentation.gallery

import android.content.res.Configuration
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.myapplication.R
import com.example.myapplication.domain.models.Image
import com.example.myapplication.presentation.base.app.GalleryApp
import com.example.myapplication.presentation.base.mvi.BaseMVIActivity
import com.example.myapplication.presentation.gallery.epoxy.ImagesController
import com.example.myapplication.presentation.gallery.epoxy.NumItemsInGridRow
import com.example.myapplication.presentation.tools.ScrollToBottomDetector
import kotlinx.android.synthetic.main.activity_gallery.*
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.pure.AppNavigator

class GalleryActivity : GalleryView,
	BaseMVIActivity<GalleryView.GalleryIntent, GalleryView, GalleryPresenter>() {

	override val presenter: GalleryPresenter
		get() = presentersComponent.galleryPresenter()

	private lateinit var controller: ImagesController

	private val navigatorHolder: NavigatorHolder
		get() = (applicationContext as GalleryApp).navigationComponent.navigatorHolder()

	private val navigator = AppNavigator(this, 0/*just stub here*/)

	override fun onCreate(savedInstanceState: Bundle?) {
		setContentView(R.layout.activity_gallery)

		val numItemsInGridRow = getNumItemsInGridRow()
		val layoutManager = GridLayoutManager(this, numItemsInGridRow.numItemsForCurrentOrientation)
		controller = ImagesController(
			{ intents.onNext(GalleryView.GalleryIntent.ImageSelected(it)) },
			numItemsInGridRow
		)
		controller.spanCount = numItemsInGridRow.numItemsForCurrentOrientation
		layoutManager.spanSizeLookup = controller.spanSizeLookup
		epoxyRecycler.layoutManager = layoutManager
		epoxyRecycler.setController(controller)
		epoxyRecycler.addOnScrollListener(ScrollToBottomDetector {
			if (!controller.showLoading && controller.retryLambda == null) {
				intents.onNext(GalleryView.GalleryIntent.NextPage)
			}
		})

		if (savedInstanceState == null) {
			intents.onNext(GalleryView.GalleryIntent.Start)
		}

		super.onCreate(savedInstanceState)
	}

	override fun onResume() {
		super.onResume()
		navigatorHolder.setNavigator(navigator)
	}

	override fun onPause() {
		super.onPause()
		navigatorHolder.removeNavigator()
	}

	override fun showNetworkError(show: Boolean) {
		if (show) {
			controller.retryLambda = { intents.onNext(GalleryView.GalleryIntent.NextPage) }
		} else {
			controller.retryLambda = null
		}
	}

	override fun showImages(images: List<Image>) {
		controller.images = images
	}

	override fun showLoading(show: Boolean) {
		controller.showLoading = show
	}

	private fun getNumItemsInGridRow() = when (resources.configuration.orientation) {
		Configuration.ORIENTATION_PORTRAIT -> NumItemsInGridRow.forPortrait()
		Configuration.ORIENTATION_LANDSCAPE -> NumItemsInGridRow.forLandscape()
		else -> NumItemsInGridRow.forPortrait()
	}
}