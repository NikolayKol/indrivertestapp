package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.data.network.UnsplashApi
import com.example.myapplication.data.repositories.ImagesRepositoryImpl
import com.example.myapplication.domain.repositories.ImagesRepository
import dagger.Module
import dagger.Provides

@Module
class RepositoriesModule {
	@Provides
	fun imagesRepository(api: UnsplashApi): ImagesRepository {
		return ImagesRepositoryImpl(api)
	}
}