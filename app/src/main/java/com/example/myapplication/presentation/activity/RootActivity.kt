package com.example.myapplication.presentation.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.presentation.base.app.GalleryApp
import com.example.myapplication.presentation.base.di.components.NavigationComponent
import ru.terrakok.cicerone.android.support.SupportAppNavigator

class RootActivity : AppCompatActivity() {

	private val navigationComponent: NavigationComponent
		get() = (application as GalleryApp).navigationComponent

	private val navigator = object : SupportAppNavigator(this, R.id.container) {
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_root)
	}

	override fun onResume() {
		super.onResume()
		navigationComponent.navigatorHolder().setNavigator(navigator)
	}

	override fun onPause() {
		super.onPause()
		navigationComponent.navigatorHolder().removeNavigator()
	}


}