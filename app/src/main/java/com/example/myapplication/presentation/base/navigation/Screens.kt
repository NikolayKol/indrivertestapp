package com.example.myapplication.presentation.base.navigation

import android.content.Context
import android.content.Intent
import com.example.myapplication.presentation.image.ImageActivity
import ru.terrakok.cicerone.android.pure.AppScreen

class ImageScreen(private val imageUrl: String) : AppScreen() {
	override fun getActivityIntent(context: Context): Intent {
		return ImageActivity.createIntent(context, imageUrl)
	}
}