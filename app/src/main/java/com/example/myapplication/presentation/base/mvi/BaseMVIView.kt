package com.example.myapplication.presentation.base.mvi

import io.reactivex.Observable

interface BaseMVIView<Intent> {
	val intents: Observable<Intent>
}