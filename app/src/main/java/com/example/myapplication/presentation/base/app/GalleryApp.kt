package com.example.myapplication.presentation.base.app

import android.app.Application
import com.example.myapplication.presentation.base.di.components.*
import com.example.myapplication.presentation.base.di.modules.*
import ru.terrakok.cicerone.Cicerone

class GalleryApp : Application() {

	private val appComponent: AppComponent by lazy {
		DaggerAppComponent
			.builder()
			.appDataModule(AppDataModule(this))
			.retrofitModule(RetrofitModule(this))
			.build()
	}

	private val useCasesComponent: UseCaseComponent by lazy {
		DaggerUseCaseComponent
			.builder()
			.appComponent(appComponent)
			.apiModule(ApiModule())
			.repositoriesModule(RepositoriesModule())
			.build()
	}

	val navigationComponent: NavigationComponent by lazy {
		DaggerNavigationComponent
			.builder()
			.navigationModule(NavigationModule(Cicerone.create()))
			.build()
	}

	val presentersComponent: PresentersComponent by lazy {
		DaggerPresentersComponent
			.builder()
			.useCaseComponent(useCasesComponent)
			.navigationComponent(navigationComponent)
			.presentersModule(PresentersModule())
			.build()
	}
}