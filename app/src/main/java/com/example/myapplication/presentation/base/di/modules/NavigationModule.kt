package com.example.myapplication.presentation.base.di.modules

import com.example.myapplication.presentation.base.di.NavigationScope
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module
class NavigationModule(private val cicerone: Cicerone<Router>) {

	@Provides
	@NavigationScope
	fun router(): Router {
		return cicerone.router
	}

	@Provides
	@NavigationScope
	fun navigatorHolder(): NavigatorHolder {
		return cicerone.navigatorHolder
	}

}