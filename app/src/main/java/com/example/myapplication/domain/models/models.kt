package com.example.myapplication.domain.models


data class Image(val id: String, val smallImage: String, val bigImage: String)