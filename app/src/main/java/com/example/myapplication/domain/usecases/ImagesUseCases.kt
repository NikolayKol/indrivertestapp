package com.example.myapplication.domain.usecases

import com.example.myapplication.domain.models.Image
import com.example.myapplication.domain.repositories.ImagesRepository
import io.reactivex.Single
import javax.inject.Inject

class ImagesUseCases
@Inject
constructor(private val imagesRepository: ImagesRepository) {

	fun getImagesPage(page: Int): Single<List<Image>> {
		return imagesRepository.loadImages(page, PAGE_SIZE)
	}

	companion object {
		private const val PAGE_SIZE = 20
	}
}