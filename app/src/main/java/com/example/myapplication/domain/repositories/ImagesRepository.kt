package com.example.myapplication.domain.repositories

import com.example.myapplication.domain.models.Image
import io.reactivex.Single

interface ImagesRepository {
	fun loadImages(page: Int, quantity: Int): Single<List<Image>>
}